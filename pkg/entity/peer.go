package entity

type Peer struct {
	PublicKey   []byte            `json:"public_key"`
	OperatorKey []byte            `json:"operator_key"`
	Remarks     map[string]string `json:"remarks"`
	Endpoints   []Endpoint        `json:"endpoints"`
}

type Endpoint struct {
	SendPort      int    `json:"send_port"`
	AddressFamily string `json:"address_family"`
	Address       string `json:"address"`
	MTU           int    `json:"mtu"`
}

type PeerEnvelope struct {
	Peer
	Signature string `json:"signature"`
}
