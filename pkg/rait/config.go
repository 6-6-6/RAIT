package rait

type RAIT struct {
	Registry    string      `hcl:"registry,attr"`      // mandatory, registry url
	OperatorKey string      `hcl:"operator_key,attr"`  // mandatory, private key of node operator
	PrivateKey  string      `hcl:"private_key,attr"`   // mandatory, wireguard private key
	Namespace   string      `hcl:"namespace,optional"` // optional, wireguard interface namespace
	Transport   []Transport `hcl:"transport,block"`    // mandatory, wireguard socket params

	Babeld  *Babeld           `hcl:"babeld,block"`     // optional, integration with babeld
	Remarks map[string]string `hcl:"remarks,optional"` // optional, public metadata key value map
}

type Transport struct {
	AddressFamily string `hcl:"address_family,attr"` // mandatory, socket address family, ip4 or ip6
	Address       string `hcl:"address,optional"`    // optional, address to advertise
	BindAddress   string `hcl:"bind_addr,optional"`  // optional, address to bind
	SendPort      int    `hcl:"send_port,attr"`      // mandatory, socket send port

	Namespace string `hcl:"namespace,optional"` // optional, wireguard interface creation namespace
	MTU       int    `hcl:"mtu,attr"`           // mandatory, interface mtu
	IFPrefix  string `hcl:"ifprefix,attr"`      // mandatory, interface naming prefix
	IFGroup   int    `hcl:"ifgroup,attr"`       // mandatory, interface group

	FwMark     int  `hcl:"fwmark,optional"`      // optional, fwmark set on out going packets
	RandomPort bool `hcl:"random_port,optional"` // optional, whether to randomize listen port
}

type Babeld struct {
	Enabled    bool   `hcl:"enabled,optional"`     // mandatory, whether to enable babeld integration
	SocketType string `hcl:"socket_type,optional"` // mandatory, control socket type, tcp or unix
	SocketAddr string `hcl:"socket_addr,optional"` // mandatory, control socket address
	Param      string `hcl:"param,optional"`       // optional, interface param
	Footnote   string `hcl:"footnote,optional"`    // optional, ending command
}
