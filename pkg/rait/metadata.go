package rait

import (
	"crypto/ed25519"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"gitlab.com/NickCao/RAIT/v4/pkg/entity"
	"gitlab.com/NickCao/RAIT/v4/pkg/misc"
	"golang.zx2c4.com/wireguard/wgctrl/wgtypes"
	"gopkg.in/square/go-jose.v2"
)

func (r *RAIT) Metadata() (string, error) {
	var p entity.Peer

	wgsk, err := wgtypes.ParseKey(r.PrivateKey)
	if err != nil {
		return "", err
	}
	wgpk := wgsk.PublicKey()
	p.PublicKey = wgpk[:]

	opsk, err := base64.StdEncoding.DecodeString(r.OperatorKey)
	if err != nil {
		return "", err
	}
	if len(opsk) != ed25519.SeedSize {
		return "", fmt.Errorf("wrong key size")
	}
	oppk := ed25519.NewKeyFromSeed(opsk).Public().(ed25519.PublicKey)
	p.OperatorKey = oppk

	p.Remarks = r.Remarks

	for _, t := range r.Transport {
		p.Endpoints = append(p.Endpoints, entity.Endpoint{
			SendPort:      t.SendPort,
			AddressFamily: t.AddressFamily,
			Address:       t.Address,
			MTU:           t.MTU,
		})
	}

	payload, err := json.Marshal(&p)
	if err != nil {
		return "", err
	}

	signer, err := jose.NewSigner(jose.SigningKey{
		Algorithm: jose.EdDSA,
		Key:       ed25519.NewKeyFromSeed(opsk),
	}, nil)
	if err != nil {
		return "", err
	}

	sig, err := signer.Sign(payload)
	if err != nil {
		return "", err
	}

	detached, err := sig.DetachedCompactSerialize()
	if err != nil {
		return "", err
	}

	meta, err := json.Marshal(entity.PeerEnvelope{
		Peer:      p,
		Signature: detached,
	})
	if err != nil {
		return "", err
	}

	return string(meta), nil
}

func (r *RAIT) Listing() ([]entity.Peer, error) {
	reader, err := misc.NewReadCloser(r.Registry)
	if err != nil {
		return nil, err
	}
	defer reader.Close()

	var peers []entity.Peer
	err = json.NewDecoder(reader).Decode(&peers)
	if err != nil {
		return nil, err
	}

	for i, p := range peers {
		for j, ep := range p.Endpoints {
			if ep.MTU == 0 {
				peers[i].Endpoints[j].MTU = 1400
			}
		}
	}

	return peers, nil
}
