package main

import (
	"crypto/ed25519"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	"github.com/urfave/cli/v2"
	"gitlab.com/NickCao/RAIT/v4/pkg/entity"
	"gitlab.com/NickCao/RAIT/v4/pkg/rait"
	"go.uber.org/zap"
	"golang.zx2c4.com/wireguard/wgctrl/wgtypes"
	"gopkg.in/square/go-jose.v2"
	"io/ioutil"
	"net/http"
	"os"
)

var Version string
var r *rait.RAIT

var commonFlags = []cli.Flag{
	&cli.StringFlag{
		Name:    "config",
		Usage:   "path to configuration file",
		Aliases: []string{"c"},
		Value:   "/etc/rait/rait.conf",
	},
}

var commonBeforeFunc = func(ctx *cli.Context) error {
	var err error
	r, err = rait.NewRAIT(ctx.String("config"))
	if err != nil {
		return err
	}
	return nil
}

func main() {
	app := &cli.App{
		Name:    "rait",
		Usage:   "redundant array of inexpensive tunnels",
		Version: Version,
		Flags: []cli.Flag{
			&cli.BoolFlag{
				Name:    "debug",
				Aliases: []string{"d"},
				Usage:   "enable debug level log",
				Value:   false,
			},
		},
		Before: func(ctx *cli.Context) error {
			config := zap.NewDevelopmentConfig()
			config.DisableStacktrace = true
			config.DisableCaller = true
			if !ctx.Bool("debug") {
				config.Level.SetLevel(zap.InfoLevel)
			}
			logger, err := config.Build()
			if err != nil {
				panic(err)
			}
			zap.ReplaceGlobals(logger)
			return nil
		},
		Commands: []*cli.Command{{
			Name:    "up",
			Aliases: []string{"u", "sync"},
			Usage:   "create or sync the tunnels",
			Flags:   commonFlags,
			Before:  commonBeforeFunc,
			Action: func(ctx *cli.Context) error {
				return r.Sync(true)
			},
		}, {
			Name:    "down",
			Aliases: []string{"d"},
			Usage:   "destroy the tunnels",
			Flags:   commonFlags,
			Before:  commonBeforeFunc,
			Action: func(ctx *cli.Context) error {
				return r.Sync(false)
			},
		}, {
			Name:  "metrics",
			Usage: "serve prometheus metrics",
			Flags: append(commonFlags, &cli.StringFlag{
				Name:    "listen",
				Aliases: []string{"l"},
				Usage:   "listen address",
				Value:   ":9101",
			}),
			Before: commonBeforeFunc,
			Action: func(ctx *cli.Context) error {
				prometheus.MustRegister(r.Babeld)
				http.Handle("/metrics", promhttp.Handler())
				return http.ListenAndServe(ctx.String("listen"), nil)
			},
		}, metaCommands, keyCommands},
		HideHelpCommand: true,
	}
	if err := app.Run(os.Args); err != nil {
		zap.S().Error(err)
		os.Exit(1)
	}
}

var metaCommands = &cli.Command{
	Name:    "meta",
	Aliases: []string{"m"},
	Usage:   "metadata operations",
	Subcommands: []*cli.Command{
		{
			Name:    "export",
			Aliases: []string{"x"},
			Usage:   "export node metadata",
			Before:  commonBeforeFunc,
			Flags:   commonFlags,
			Action: func(ctx *cli.Context) error {
				meta, err := r.Metadata()
				if err != nil {
					return err
				}
				_, err = fmt.Println(meta)
				return err
			},
		},
		{
			Name:    "validate",
			Aliases: []string{"v"},
			Usage:   "validate node metadata",
			Action: func(ctx *cli.Context) error {
				s, err := ioutil.ReadAll(os.Stdin)
				if err != nil {
					return err
				}

				var e entity.PeerEnvelope
				err = json.Unmarshal(s, &e)
				if err != nil {
					return err
				}

				payload, err := json.Marshal(&e.Peer)
				if err != nil {
					return err
				}

				sig, err := jose.ParseDetached(e.Signature, payload)
				if err != nil {
					return err
				}

				_, err = sig.Verify(ed25519.PublicKey(e.OperatorKey))
				if err != nil {
					return err
				}

				_, err = fmt.Println(string(payload))
				return err
			},
		},
	},
	HideHelpCommand: true,
}

var keyCommands = &cli.Command{
	Name:    "key",
	Aliases: []string{"k"},
	Usage:   "key operations",
	Subcommands: []*cli.Command{
		{
			Name:    "generate",
			Aliases: []string{"g"},
			Usage:   "generate private key",
			Action: func(ctx *cli.Context) error {
				_, key, err := ed25519.GenerateKey(nil)
				if err != nil {
					return err
				}
				_, err = fmt.Println(base64.StdEncoding.EncodeToString(key[:32]))
				return err
			},
		},
		{
			Name:    "curve25519pub",
			Aliases: []string{"cp"},
			Usage:   "curve25519 public key",
			Action: func(ctx *cli.Context) error {
				var sk string
				_, err := fmt.Scanln(&sk)
				if err != nil {
					return err
				}
				key, err := wgtypes.ParseKey(sk)
				if err != nil {
					return err
				}
				_, err = fmt.Println(key.PublicKey().String())
				return err
			},
		},
		{
			Name:    "ed25519pub",
			Aliases: []string{"ep"},
			Usage:   "ed25519 public key",
			Action: func(c *cli.Context) error {
				var sk string
				_, err := fmt.Scanln(&sk)
				if err != nil {
					return err
				}
				seed, err := base64.StdEncoding.DecodeString(sk)
				if err != nil {
					return err
				}
				if len(seed) != ed25519.SeedSize {
					return fmt.Errorf("wrong key size")
				}
				_, err = fmt.Println(base64.StdEncoding.
					EncodeToString(ed25519.NewKeyFromSeed(seed).Public().(ed25519.PublicKey)))
				return err
			},
		},
	},
	HideHelpCommand: true,
}
